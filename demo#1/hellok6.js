export const options = {
  thresholds: {
    // Assert that 99% of requests finish within 3000ms.
    "http_req_duration{my_tag: \"I'm a tag\"}": ["p(99) < 3000"],
  },
  // Ramp the number of virtual users up and down
  stages: [
    { duration: "2s", target: 15 },
    { duration: "5s", target: 30 },
    { duration: "5s", target: 0 },
  ],
};

import http from 'k6/http';
import { check } from 'k6';

// Simulated user behavior
export default function () {
  let res = http.get("https://test-api.k6.io/public/crocodiles/1/", { tags: { my_tag: "I'm a tag" } });
  // Validate response status
  check(res, { "status was 200": (r) => r.status == 200 }, { my_tag: "I'm a tag" });
};
